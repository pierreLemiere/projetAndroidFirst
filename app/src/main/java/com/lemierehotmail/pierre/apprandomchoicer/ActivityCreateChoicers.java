package com.lemierehotmail.pierre.apprandomchoicer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.util.Log;

/**
 * Created by pierr on 01/03/2018.
 */

//Class qui permet de choisir entre l'ajout de joueurs ou d'activités
public class ActivityCreateChoicers extends MainActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_choicers);
    }

    //fonction pour lancer l'activity pour ajouter des joueurs
    public void lancer_activite_create1(View view)
    {
        Log.i("SimpleIntentActivity", "button pressed");
        startActivity(new Intent(this, ActivityCreatePlayers.class));
    }

    //fonction pour lancer l'activity pour ajouter des activités
    public void lancer_activite_create2(View view)
    {
        Log.i("SimpleIntentActivity", "button pressed");
        startActivity(new Intent(this, ActivityCreateActivities.class));
    }
}

