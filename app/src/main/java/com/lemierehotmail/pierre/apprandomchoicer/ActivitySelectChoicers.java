package com.lemierehotmail.pierre.apprandomchoicer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

//Class qui permet de choisir entre la sélection de joueurs ou d'activités
public class ActivitySelectChoicers extends MainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_choicers);
    }

    //fonction qui permet de lancer l'activity pour sélectionner des joueurs
    public void lancer_activite_select1(View view)
    {
        Log.i("SimpleIntentActivity", "button pressed");
        startActivity(new Intent(this, ActivitySelectPlayers.class));
    }

    //fonction qui permet de lancer l'activity pour sélectionner des activités
    public void lancer_activite_select2(View view)
    {
        Log.i("SimpleIntentActivity", "button pressed");
        startActivity(new Intent(this, ActivitySelectActivities.class));
    }
}

