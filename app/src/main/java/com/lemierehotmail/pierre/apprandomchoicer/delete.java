package com.lemierehotmail.pierre.apprandomchoicer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

//Class qui demande à l'utilisateur de supprimer ou non la BD
public class delete extends MainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete);
    }

    //fonction qui reset la BD et qui renvoie l'utilisateur vers l'activity d'acceuil
    public void suppressBd(View view)
    {
        Log.i("SimpleIntentActivity", "button pressed");
        this.deleteDatabase(this.get_BD().getBdName());
        startActivity(new Intent(this, MainActivity.class));
    }

    //fonction qui renvoie l'utilisateur vers l'activity d'acceuil
    public void lancer_main_activite(View view)
    {
        Log.i("SimpleIntentActivity", "button pressed");
        startActivity(new Intent(this, MainActivity.class));
    }
}
