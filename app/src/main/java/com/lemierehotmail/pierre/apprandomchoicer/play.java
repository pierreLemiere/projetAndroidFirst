package com.lemierehotmail.pierre.apprandomchoicer;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class play extends MainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
    }

    //fonction qui permet de jouer à l'application
    public void doScroll(View view){
        //Recherche d'un joueur aléatoirement
        Cursor cursorP = this.get_BD().getCursorP();
        int cptP = 0; //compteur de joueurs pour connaitre le nombre d'occurence dans la BD
        cursorP.moveToFirst();
        while(!cursorP.isAfterLast()){
            cptP +=1;
            cursorP.moveToNext();
        }
        cursorP.moveToFirst();
        Random r1 = new Random();
        int idP = r1.nextInt(cptP) +1;
        //une fois un id de joueur choisi aléatoirement on retourne chercher le joueur
        while(!cursorP.isAfterLast()){
            if (cursorP.getInt(0)==idP){
                TextView txt = (TextView) findViewById(R.id.txtPlayer);
                txt.setText(cursorP.getString(1));
            }
            cursorP.moveToNext();
        }
        cursorP.close();

        //Recherche d'une activité aléatoirement
        Cursor cursorA = this.get_BD().getCursorA();
        int cptA = 0; //compteur d'activités pour connaitre le nombre d'occurence dans la BD
        cursorA.moveToFirst();
        while(!cursorA.isAfterLast()){
            cptA +=1;
            cursorA.moveToNext();
        }
        cursorA.moveToFirst();
        Random r2 = new Random();
        int idA = r2.nextInt(cptA) +1;
        //une fois un id d'activité choisi aléatoirement on retourne chercher l'activité
        while(!cursorA.isAfterLast()){
            if (cursorA.getInt(0)==idA){
                TextView txt = (TextView) findViewById(R.id.txtAction);
                txt.setText(cursorA.getString(1));
            }
            cursorA.moveToNext();
        }
        cursorA.close();
    }
}
