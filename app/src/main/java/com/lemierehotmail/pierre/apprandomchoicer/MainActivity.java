package com.lemierehotmail.pierre.apprandomchoicer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

//Class qui représente l'acceuil et propose toutes les fonctionnalités possibles
public class MainActivity extends AppCompatActivity {

    private BDApp dbopenhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Création/Ouverture de la base de données.
        dbopenhelper = new BDApp(this);
    }

    //fonction qui lance l'activity pour ajouter des choix
    public void lancer_activite1(View view)
    {
        Log.i("SimpleIntentActivity", "button pressed");
        startActivity(new Intent(this, ActivityCreateChoicers.class));
    }

    //fonction qui lance l'activity pour sélectionner des choix
    public void lancer_activite2(View view)
    {
        Log.i("SimpleIntentActivity", "button pressed");
        startActivity(new Intent(this, ActivitySelectChoicers.class));
    }

    //fonction qui lance l'activity pour Jouer
    public void lancer_activite3(View view)
    {
        Log.i("SimpleIntentActivity", "button pressed");
        startActivity(new Intent(this, play.class));
    }

    //fonction qui lance l'activity pour supprimer la BD
    public void lancer_activite4(View view)
    {
        Log.i("SimpleIntentActivity", "button pressed");
        startActivity(new Intent(this, delete.class));
    }

    public BDApp get_BD(){
        return this.dbopenhelper;
    }
}
