package com.lemierehotmail.pierre.apprandomchoicer;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

//Class pour Sélectionner les activités que l'on veut utiliser
public class ActivitySelectActivities extends MainActivity {

    private Cursor cursor;
    private SimpleCursorAdapter sca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_activities);

        //récupération des données dans la bd et affichage dans une listView
        ListView lv0 = (ListView)findViewById( R.id.lv0 );

        cursor = this.get_BD().getCursorA();

        String[] columns = {this.get_BD().getColNameA()};

        int[] views = {R.id.txt};

        sca = new SimpleCursorAdapter(this,
                R.layout.choice_list,
                cursor,
                columns,
                views,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        );

        lv0.setAdapter( sca );
    }
}
