package com.lemierehotmail.pierre.apprandomchoicer;

import android.widget.ImageView;

/**
 * Created by pierr on 07/03/2018.
 */

//Class Player représentant un Joueur
public class Player {

    private String nom;
    private ImageView photo;

    public Player(String nom){
        this.nom = nom;
    }

    public Player(String nom, ImageView photo){
        this.nom = nom;
        this.photo = photo;
    }

    public String getNom(){
        return this.nom;
    }

    public void setNom(String nom){
        this.nom = nom;
    }

    public ImageView getPhoto(){
        return this.photo;
    }

    public void setPhoto(ImageView photo){
        this.photo = photo;
    }
}
