package com.lemierehotmail.pierre.apprandomchoicer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.SimpleCursorAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pierr on 01/03/2018.
 */

public class BDApp extends SQLiteOpenHelper{

    private static final String BD_NAME = "BD";             // Nom de la base.

    //players
    private static final String BD_TABLE_PLAYERS = "table_players";  // Nom de la table.
    private static final String COL_NAME_P = "NAME_P";

    //Activities
    private static final String BD_TABLE_ACTIVITIES = "table_activities";
    private static final String COL_NAME_A = "NAME_A";

    private SQLiteDatabase db;                              // Base de données

    private Context context;

    public BDApp(Context context) {
        // Appel au constructeur qui s'occupe de créer ou ouvrir la base.
        super(context, BD_NAME, null, 2);
        this.context = context;
        // Récupération de la base de données.
        db = getWritableDatabase();
    }

    /**
     * Méthode appelée si la base n'existe pas.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + BD_TABLE_PLAYERS+ " (_id integer primary key autoincrement,"+ COL_NAME_P + " text not null);");
        db.execSQL("create table " + BD_TABLE_ACTIVITIES+ " (_id integer primary key autoincrement,"+ COL_NAME_A + " text not null);");
    }

    /**
     * Méthode pour passer d'une version de SQLite à une nouvelle version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldversion, int newversion) {

    }

    /**
     * Insertion d'un Player dans la table.
     */
    public void insertValueP(Player player) {
        // La chaîne n'est pas directement ajoutée dans la base.
        // Il faut passer par la création d'une structure intermédiaire ContenValues.
        ContentValues content = new ContentValues();
        // Insertion de la chaîne dans l'instance de ContentValues.
        content.put(COL_NAME_P, player.getNom());

        // Insertion dans la base de l'instance de ContentValues contenant la chapine.
        db.insert(BD_TABLE_PLAYERS, null, content);
    }

    /**
     * Insertion d'une Activities dans la table.
     */
    public void insertValueA(Activities acti) {
        // La chaîne n'est pas directement ajoutée dans la base.
        // Il faut passer par la création d'une structure intermédiaire ContenValues.
        ContentValues content = new ContentValues();
        // Insertion de la chaîne dans l'instance de ContentValues.
        content.put(COL_NAME_A, acti.getNom());

        // Insertion dans la base de l'instance de ContentValues contenant la chapine.
        db.insert(BD_TABLE_ACTIVITIES, null, content);
    }

    /**
     * Modification d'un player.
     */
    public int updatePlayer(int id, Player player){
        //il faut simplement préciser quel player on doit mettre à jour grâce à l'ID
        ContentValues content = new ContentValues();
        content.put(COL_NAME_P, player.getNom());
        return db.update(BD_TABLE_PLAYERS, content, "_id" + " = " +id, null);
    }

    public int removePlayerWithID(int id){
        //Suppression d'un player de la BDD grâce à l'ID
        return db.delete(BD_TABLE_PLAYERS, "_id" + " = " +id, null);
    }


    /**
     * Récupération des chaînes de la table.
     */
    public List<String> getValues() {
        List<String> list = new ArrayList<String>();
        String[] columns = {COL_NAME_P};
        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = db.query(BD_TABLE_PLAYERS, columns, null, null, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            list.add(cursor.getString(0));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return list;
    }

    public Cursor getCursorP()
    {
        String[] columns = {"_id", COL_NAME_P};

        Cursor cursor = db.query(BD_TABLE_PLAYERS, columns, null, null, null, null, null);
        //cursor.moveToFirst();

        Log.d("SQLiteDemo", "cursor " + cursor);

        return cursor;
    }

    public Cursor getCursorA()
    {
        String[] columns = {"_id", COL_NAME_A};

        Cursor cursor = db.query(BD_TABLE_ACTIVITIES, columns, null, null, null, null, null);
        //cursor.moveToFirst();

        Log.d("SQLiteDemo", "cursor " + cursor);

        return cursor;
    }

    public SimpleCursorAdapter getAdapterP()
    {
        String[] columns = {COL_NAME_P};
        int[] views = { R.id.txt };
        Cursor cursor = db.query(BD_TABLE_PLAYERS, columns, null, null, null, null, null);
        //cursor.moveToFirst();

        return new SimpleCursorAdapter(context,
                R.layout.choice_list,
                cursor,
                columns,
                views);

    }

    public SimpleCursorAdapter getAdapterA()
    {
        String[] columns = {COL_NAME_A};
        int[] views = { R.id.txt };
        Cursor cursor = db.query(BD_TABLE_ACTIVITIES, columns, null, null, null, null, null);
        //cursor.moveToFirst();

        return new SimpleCursorAdapter(context,
                R.layout.choice_list,
                cursor,
                columns,
                views);

    }

    public String getColNameP(){
        return this.COL_NAME_P;
    }

    public String getColNameA(){
        return this.COL_NAME_A;
    }

    public String getBdName(){ return this.BD_NAME;}

}
