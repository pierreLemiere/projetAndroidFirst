package com.lemierehotmail.pierre.apprandomchoicer;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

//Class qui permet d'ajouter de nouveaux joueurs
public class ActivityCreatePlayers extends MainActivity {

    private Cursor cursor;
    private SimpleCursorAdapter sca;

    private EditText et0;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_players);

        //récupération des données dans la bd et affichage dans une listView
        et0 = (EditText)findViewById(R.id.et0);

        ListView lv0 = (ListView)findViewById( R.id.lv0 );

        cursor = this.get_BD().getCursorP();

        String[] columns = {this.get_BD().getColNameP()};

        int[] views = {R.id.txt};

        sca = new SimpleCursorAdapter(this,
                R.layout.choice_list,
                cursor,
                columns,
                views,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        );

        lv0.setAdapter( sca );
    }

    public void insertIntoBD( View v )
    {
        // Insertion dans la base de données.
        if( et0.getText().length() != 0 )
        {
            this.get_BD().insertValueP( new Player(et0.getText().toString()) );
            cursor.requery();
            sca.notifyDataSetChanged();
        }
        // Suppression du nom
        EditText txt = (EditText) findViewById(R.id.et0);
        txt.setText("");
    }
}
